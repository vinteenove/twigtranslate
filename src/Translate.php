<?php

namespace TwigTranslate;

use Lliure\LliurePanel\Tools\Environment as LliureEnvironment;
use Twig\Environment as TwigEnvironment;
use Twig\Extension\AbstractExtension;
use Twig\Loader\ChainLoader;
use Twig\TwigFilter;

class Translate extends AbstractExtension
{
	
	private array $cache = [];
	
	public function __construct(
		private string $locale = 'pt_BR',
		private string $localePath = __DIR__ . '/../../../etc/locales',
		private ?array $domain = null
	){
		if(empty($this->domain)){
			$this->domain = [$this->locale];
		}
	}
	
	public function getFilters()
	{
		return [
			new TwigFilter('translate', [$this, 'translate'], ['needs_environment' => true]),
			new TwigFilter('_', [$this, 'translate'], ['needs_environment' => true]),
		];
	}
	
	public function translate(TwigEnvironment|LliureEnvironment $env, $text, $variables = [])
	{
		$translatedText = $this->getTranslatedText($text);
		
		$search = array_map(fn($k) => '%' . $k . '%', array_keys($variables));
		$replace = array_map(fn($k) => '{{ ' . $k . ' }}', array_keys($variables));
		$translatedText = str_replace($search, $replace, $translatedText);
		
		$key = uniqid('TwigTranslate');
		$loader = new \Twig\Loader\ArrayLoader([$key => $translatedText]);
		$chainLoader = $env->getLoader();
		
		if($chainLoader instanceof ChainLoader){
			$chainLoader->addLoader($loader);
		}else{
			$chainLoader = (new ChainLoader([$loader, $chainLoader]));
		}
		
		$env->setLoader($chainLoader);
		$render = $env->render($key, $variables);
		
		if($env instanceof LliureEnvironment){
			$render = $env->html();
		}
		
		return $render;
	}
	
	private function getTranslatedText(string $originalText): string
	{
		$originalText = trim(preg_replace('/\s+/', ' ', $originalText));
		$translations = [];
		foreach($this->domain as $domain){
			if(isset($this->cache[$this->locale][$domain])){
				$translations += $this->cache[$this->locale][$domain];
			}else{
				$yamlFile = $this->localePath . "/" . $this->locale . "/" . $domain . ".yaml";
				if(file_exists($yamlFile)){
					$this->cache[$this->locale][$domain] = \yaml_parse_file($yamlFile)?: [];
					$translations += $this->cache[$this->locale][$domain];
				}
			}
		}
		
		if(isset($translations[$originalText])){
			return $translations[$originalText];
		}
		
		return $originalText;
	}
	
}