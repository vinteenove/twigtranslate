<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once '../vendor/autoload.php';

use Twig\TwigFilter;

$loader = new \Twig\Loader\FilesystemLoader('views');
$twig = new \Twig\Environment($loader, [
    'debug' => true,
]);

$l = 'en_US';
if(isset($_GET['l'])){
    $l = $_GET['l'];
}

$myTwigFilters = new \TwigTranslate\Translate($l, __DIR__ . "/etc/locales");
$twig->addExtension($myTwigFilters);

echo $twig->render('home.html', ['name' => 'Jeison']);

